"""For getting a random int"""
from random import randint
import json

import requests

def getRandomPost():
    """makes a request for the most latest comic, takes the comic number, and sets it as a max int to get a random comic"""
    returnValue = []
    response = requests.get('https://xkcd.com/info.0.json')
    getRandomLimit = json.loads(response.text)
    randpost = str(randint(1, getRandomLimit["num"]))
    postResponse = requests.get('http://xkcd.com/' + randpost + '/info.0.json')
    postResponse = json.loads(postResponse.text)
    returnValue.append(postResponse["img"])
    returnValue.append(postResponse["alt"])


    return returnValue