"""This module is used to get a random number"""
from random import randint
import os
import discord
from discord.ext import commands

import XKCD

botVer = os.environ.get('CI_COMMIT_SHORT_SHA')
Client = discord.Client()
client = commands.Bot(command_prefix='!')
client.remove_command('help')
def __init__(self, bot):
    self.bot = bot
@client.event
async def on_ready():
    """This gets called when the bot is ready to accept commands"""
    #await client.change_presence(game=discord.Game(name='mention me!', type=1))
    print("Bot successfully launched.")

@client.command(pass_context=True)
async def joinedAt(ctx):
    """Sends the date the the user joined a server"""
    await ctx.channel.send(ctx.message.author.mention + " joined the channel at: " + ctx.message.author.joined_at.strftime('%Y/%m/%d') + " `Y/M/D`")

@client.event
async def on_message(ctx):
    """Gets called on every message sent"""
    if not ctx.author.bot:
        if ctx.content == "<@547010966475505664>" or ctx.content.lower() == "skype":
            await ctx.channel.send("Hey " + ctx.author.mention + " I was created by <@142714319673622528> , you can do ´!help Skype´ to see what commands I have.")
        if ctx.content.lower() == "!help" or ctx.content.lower() == "!help!skype":
            link = "<https://gitlab.com/BoxBop/discord-bot>"
            await ctx.channel.send("My creator is <@142714319673622528> and my commands are `!who`, `!rand [min]-[max]` and `!xkcd`, here's my code: " + link)
        if "!rand" in ctx.content:
            number = ctx.content.replace("!rand ", "")
            halves = number.split("-")
            rannum = str(randint(int(halves[0]), int(halves[1])))
            await ctx.channel.send(ctx.author.mention + " rolled from " + number + " and got " + rannum)
        if ctx.content.lower() == "!xkcd":
            result = XKCD.getRandomPost()
            em = discord.Embed(
                title=result[1]
            )
            
            em.set_image(url=result[0])
            await ctx.channel.send(embed=em)
        if ctx.content == "!who":
            await ctx.channel.send("Hey " + ctx.author.mention + " I was created by <@142714319673622528> , my latest commit hash is (" + botVer + ") and my code can be seen here: <https://gitlab.com/BoxBop/discord-bot>")

    #await client.process_commands(ctx)
#@client.command(pass_context=True)
#async def who(ctx):
    #"""Information command, for some unknown reason it doesn't currently work as a command"""
    #text =  "Hey " + ctx.author.mention + " I was created by <@142714319673622528> , my latest commit hash is (" + botVer + ") and my code can be seen here: <https://gitlab.com/BoxBop/discord-bot>"
    #await ctx.channel.send(ctx.channel.id, text)

#with open("D:/School/Software testing (eritikass)/discord-bot/token.json", "r") as read_file:
#    token = json.load(read_file)
TOKEN = os.environ.get('BOT_TOKEN')
#client.run(token["bot_token"])
client.run(TOKEN)